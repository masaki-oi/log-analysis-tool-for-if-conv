Attribute VB_Name = "Main"
Option Explicit

Public Const MAIN_SHEET_NAME As String = "Menu"
Public Const MANUAL_SHEET_NAME As String = "Manual"
Public Const RNOTE_SHEET_NAME As String = "RNote"
Public Const INTEG_SHEET_NAME As String = "All"
Public Const SEQ_SHEET_NAME As String = "Seq"
Public Const SLOG_SHEET_NAME As String = "SLog"
Public Const RNG_FILE_PATH As String = "FilePath"
Public Const RNG_FILE_NO As String = "FileNo"
Public Const RNG_ANALYZE_FILE As String = "AnalyzeFile"


'*******************************************************************************
' 「初期化」ボタンをクリック
'
' [引数]
'   なし
'
' [戻り値]
'   なし
'
' [処理概要]
' (Step 1)Label1のキャプションを初期化。
' (Step 2)読込んだログの出力シートを全削除。
' (Step 3)シーケンス図、整形表示シートを削除。
'*******************************************************************************
Sub btnInitAll()

  ' Menu/Manualシート以外を全削除
  Call InitSheet

  ' シートへの出力先を初期化
  Call InitOutputFrame(MAIN_SHEET_NAME)

  Call UpdateLabel(MAIN_SHEET_NAME, "初期状態")

End Sub


'*******************************************************************************
' 「ファイル選択」ボタンをクリック
'
' [引数]
'   なし
'
' [戻り値]
'   なし
'
' [処理概要]
' (Step 1)ファイル選択ダイアログ[*.log]を表示し、ユーザーに解析対象ファイルを
'         選択してもらう。
'         ※キャンセルされた場合、何もしないで終了。
' (Step 2)選択されたファイルのPath、ファイル名を「Menu」シートに表示させる。
' (Step 3)「Menu」以外の全シートを削除する。
' (Step 4)選択されたファイルを読込み、１ファイル１シートとして読込む。
'         ※シート名は、読込んだ順番で数字(1〜)を付与。
' (Step 5)各シートのログをログ要素に分解(セル列として分解)して、
'         別シートに出力する。
'         ※シート名は、数字(1〜)に対応して、先頭にP(P1〜)を付与。
' (Step 6)全ファイル内容を統合したシートを作成し、時系列でソートする。
'*******************************************************************************
Sub btnSelectFileToBeAnalyzed()
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double
  Dim openFilePath As Variant
  Dim vTarget As Variant
  Dim FilePath As String
  Dim FileName() As String
  Dim FileNum As Long, i As Long


  Application.ScreenUpdating = False

  ' (Step 1)ファイル選択ダイアログ[*.log]を表示し、ユーザーに解析対象ファイルを
  '         選択してもらう。
  '         ※キャンセルされた場合、何もしないで終了。
  If (OpenMultiFiles(FilePath, FileName()) = False) Then
    Exit Sub
  End If

  ' 開始時間取得
  StartTime = Timer

  FileNum = UBound(FileName)
  Debug.Print "(btnSelectFileToBeAnalyzed):FileNum = " & FileNum

  ' シートへの出力先を初期化
  Call InitOutputFrame(MAIN_SHEET_NAME)

  ' (Step 2)選択されたファイルのPath、ファイル名を「Menu」シートに表示させる。
  ' シートへファイルPath/ファイル名を出力
  Call UpdateOutputFrame(MAIN_SHEET_NAME, FilePath, FileName())

  ' (Step 3)「Menu」,「Manual」以外の全シートを削除する。
  Call InitSheet

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Init process finished.")

  ' (Step 4)選択されたファイルを読込み、１ファイル１シートとして読込む。
  '         ※シート名は、読込んだ順番で数字(1〜)を付与。
  Call ReadLogFile2Sheet(FilePath, FileName())

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Load all logfile finished.")

  ' (Step 5)各シートのログをログ要素に分解(セル列として分解)して、
  '         別シートに出力する。
  '         ※シート名は、数字(1〜)に対応して、先頭にP(P1〜)を付与。
  For i = 1 To FileNum
    Call BreakDown2LogElement(Str(i))
  Next i

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Analyze all logfile finished.")

  ' (Step 6)全ファイル内容を統合したシートを作成し、時系列でソートする。
  Call IntegrateAllSheet(FileNum, INTEG_SHEET_NAME)

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Integrate logfile finished.")

  Call UpdateLabel(MAIN_SHEET_NAME, "ファイル読込み完了")

  ' ファイル毎に読込んだシートは非表示にする
  For i = 1 To FileNum
    Worksheets(Str(i)).Visible = False
    Worksheets(AS_SHEET & Str(i)).Visible = False
  Next i

  Worksheets(MAIN_SHEET_NAME).Activate

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "All process Complete.")

  Application.ScreenUpdating = True

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  MsgBox "Processing Complete!" & vbCrLf & _
         "elapsed time : " & TotalTime, vbInformation

End Sub

'*******************************************************************************
' 「シーケンス図作成＆ログ整形」ボタンをクリック
'
' [引数]
'   なし
'
' [戻り値]
'   なし
'
' [処理概要]
' (Step 1)ログファイルが読み込まれているかチェックする。
'         ※ログが読み込まれていない場合、強制終了。
' (Step 2)シーケンス図出力用シート、整形ログ出力用シートを作成する。
' (Step 3)「All」シートにおいて、1行ずつmsg1部分を参照して以下処理を行う。
' (Step 4)Upper Request/Response, Lower Request/Responseの場合、
'         シーケンス図出力用シート/整形ログ出力用シート両方に解析結果を出力する。
'         ※シーケンス図出力用シートの出力先セルに、整形ログ出力用シートに
'           出力したセルへのリンクを張る。
' (Step 5)上記以外の場合、シーケンス図出力用シートにのみ出力する。
' (Step 6)整形ログ出力シートにおいて、正規表現検索でerror_code値を検索し、
'         該当セルのフォント色を変更する。
'*******************************************************************************
Sub btnMakeSeqAndLogShaping()
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double


  Application.ScreenUpdating = False

  ' 開始時間取得
  StartTime = Timer

  ' (Step 1)ログファイルが読み込まれているかチェックする。
  '         ※ログが読み込まれていない場合、強制終了。
  If (CheckLogFiles() = False) Then
    MsgBox "先に「(1)ログファイル選択」で対象ログファイルを読込んで下さい", _
           vbExclamation
    Exit Sub
  End If

  ' (Step 2)シーケンス図出力用シート、整形ログ出力用シートを作成する。
  Call CreateSheet(SLOG_SHEET_NAME, INTEG_SHEET_NAME)
  Call MakeSeqSheet

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Create 2 sheets for output finished.")

  ' (Step 3)「All」シートにおいて、1行ずつmsg1部分を参照して以下処理を行う。
  ' (Step 4)Upper Request/Response, Lower Request/Responseの場合、
  '         シーケンス図出力用シート/整形ログ出力用シート両方に解析結果を出力する。
  '         ※シーケンス図出力用シートの出力先セルに、整形ログ出力用シートに
  '           出力したセルへのリンクを張る。
  ' (Step 5)上記以外の場合、シーケンス図出力用シートにのみ出力する。
  Call MakeSeqAndShapeLog(INTEG_SHEET_NAME, _
                          SEQ_SHEET_NAME, SLOG_SHEET_NAME)

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Make sequence and shape log finished.")

  ' (Step 6)整形ログ出力シートにおいて、正規表現検索でerror_code値を検索し、
  '         該当セルのフォント色を変更する。
  Call ErrorCodeCheckByRegExp(SLOG_SHEET_NAME)

  Call UpdateLabel(MAIN_SHEET_NAME, "シーケンス図作成＆ログ整形完了")

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "All process Complete.")

  Application.ScreenUpdating = True

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  MsgBox "Processing Complete!" & vbCrLf & _
         "elapsed time : " & TotalTime, vbInformation

End Sub

'*******************************************************************************
' 途中までの経過時間をデバッグ表示する
' [引数]
'   StartTime As Double ：処理開始時間
'   strDispMsg As String：経過時間の前に付与するラベル文字列
'
' [戻り値]
'   なし
'*******************************************************************************
Public Function DispElapsedTime(ByVal StartTime As Double, _
                                ByVal strDispMsg As String)
  Dim MiddleTime As Double
  Dim ProcessTime As Double

  MiddleTime = Timer
  ProcessTime = MiddleTime - StartTime
  Debug.Print strDispMsg & " : " & ProcessTime

End Function

