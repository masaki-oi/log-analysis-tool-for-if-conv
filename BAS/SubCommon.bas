Attribute VB_Name = "SubCommon"
Option Explicit

' 1ログファイルに対応した、ログ要素の分割処理結果出力用シートに付与する接頭文字定義
Public Const AS_SHEET As String = "P"
' msg1におけるREST APIログ判別用文字列の定義
Public Const UPPER_RQ_KEY As String = "UPPER REQUEST："
Public Const UPPER_RS_KEY As String = "UPPER RESPONSE："
Public Const LOWER_RQ_KEY As String = "LOWER REQUEST "
Public Const LOWER_RS_KEY As String = "LOWER RESPONSE "
' シーケンス図に記載する矢印の文字列定義
Public Const UPPER_RQ_ARROW As String = "'===>"
Public Const UPPER_RS_ARROW As String = "'<==="
Public Const LOWER_RQ_ARROW As String = "'--->"
Public Const LOWER_RS_ARROW As String = "'<---"
' Upper ResponseのJsonBody抽出用キーワード
Public Const UPPER_RS_JSON As String = ",{"
' Lower RequestのJsonBody抽出用キーワード
Public Const LOWER_RQ_JSON As String = "request:"
' Lower ResponseのJsonBody抽出用キーワード
Public Const LOWER_RS_JSON As String = ",{"
' error_codeの文字列定義
Public Const STR_ERROR_CODE As String = "error_code"

' ログ出力用シートのヘッダ列定義
Public Enum eDivHead
  Date = 1
  Time = 2
  Level = 3
  Thread1 = 4
  Thread2 = 5
  FileFuncLine = 6
  ThreadId = 7
  Service = 8
  Layer = 9
  msg1 = 10
  msg2 = 11
End Enum

' シーケンス図出力用シートのヘッダ列定義
Public Enum eSeqHead
  Date = 1
  Time = 2
  Nexas = 3
  Upper = 4
  IfConv = 5
  Lower = 6
  NeOps = 7
End Enum

' ShapeLogAndWrite関数の戻り値定義
Public Enum eReSLAW
  SUCCESS_NO_ERROR = 0
  SUCCESS_ERROR = 1
  FAIL_PARSE_ERROR = 2
  FAIL_NOT_JSON = 3
End Enum


'*******************************************************************************
' 複数ファイルの選択ダイアログを表示し、フルパス＆ファイル名を返す
'
' [引数]
'  strFilePath As String  ：選択ファイルのあるフルパスが格納される
'  strFileName() As String：選択ファイル名が格納される
' [戻り値]
'  Boolean：True  (ファイル選択された)
'         ：False (キャンセルされた)
'*******************************************************************************
Public Function OpenMultiFiles(ByRef strFilePath As String, _
                               ByRef strFileName() As String) As Boolean
  Dim openFilePath As Variant
  Dim i As Long
  Dim vTarget As Variant


  ' ダイアログボックスで選択したファイルを配列に入れる
  ' MultiSelect:=Trueにすると複数ファイル選択OK
  openFilePath = Application.GetOpenFilename("ログファイル(*.log),*.log", _
                                             , _
                                             "ログファイルを選んで下さい", _
                                             MultiSelect:=True)
  ' ファイルが１つ以上選択されていた場合、配列が返される。
  If IsArray(openFilePath) Then
    Debug.Print "(OpenMultiFiles):UBound(openFilePath) = " & UBound(openFilePath)
    ReDim strFileName(UBound(openFilePath))

    '配列分、繰り返しファイルを取得する。
    i = 1
    For Each vTarget In openFilePath
      strFileName(i) = Dir(vTarget)
      strFilePath = Replace(vTarget, strFileName(i), "")
      Debug.Print "(OpenMultiFiles):strFileName(" & i & ") = " & strFileName(i)
      i = i + 1
    Next vTarget

    Debug.Print "(OpenMultiFiles):strFilePath = " & strFilePath
  Else
    ' キャンセルされた場合、Falseを返す。
    OpenMultiFiles = False
    Exit Function
  End If

  OpenMultiFiles = True

End Function

'*******************************************************************************
' 指定シートに表示させるフルパス＆ファイル名欄を初期化する
'
' [引数]
'  sheetName As String：対象シート名
' [戻り値]
'  なし
'*******************************************************************************
Public Function InitOutputFrame(ByVal sheetName As String)
  Debug.Print "(InitOutputFrame):sheetName = " & sheetName

  With Worksheets(sheetName)
    .Range(RNG_FILE_PATH) = ""
    ' 「No.」のセルを選択し、Ctrl + Aを押した範囲の見出しを除いたセルの値クリア＆
    ' 見出し + 1行目以降の罫線をクリア
    With .Range(RNG_FILE_NO).CurrentRegion
      .Offset(1, 0).ClearContents
      .Offset(2, 0).Borders.LineStyle = False
    End With
  End With

End Function

'*******************************************************************************
' 指定シートにフルパス＆ファイル名を表示させる
'
' [引数]
'  ws As Worksheet        ：対象ワークシート
'  strFilePath As String  ：ファイルへのフルパス
'  strFileName() As String：ファイル名[配列]
' [戻り値]
'  なし
'*******************************************************************************
Public Function UpdateOutputFrame(ByVal sheetName As String, _
                                  ByVal strFilePath As String, _
                                  ByRef strFileName() As String)
  Dim i As Long

  Debug.Print "(UpdateOutputFrame):sheetName = " & sheetName & _
              " strFilePath = " & strFilePath

  With Worksheets(sheetName)
    .Range(RNG_FILE_PATH) = strFilePath

    For i = 1 To UBound(strFileName)
      Debug.Print "(UpdateOutputFrame):strFileName(" & i & ") = " & strFileName(i)

      .Range(RNG_FILE_NO).Offset(i, 0).Value = i
      .Range(RNG_ANALYZE_FILE).Offset(i, 0).Value = strFileName(i)
      ' 罫線を引く
      .Range(RNG_FILE_NO).Offset(i, 0).Borders.LineStyle = True
      .Range(RNG_ANALYZE_FILE).Offset(i, 0).Borders.LineStyle = True
    Next i
  End With

End Function

'*******************************************************************************
' 指定シートのラベルを更新する
'
' [引数]
'  strSheetName As String：シート名
'  strValue As String：更新文字列
' [戻り値]
'  なし
'*******************************************************************************
Public Function UpdateLabel(ByVal strSheetName As String, _
                            ByVal strValue As String)
  Debug.Print "(UpdateLabel):strSheetName = " & strSheetName & _
              " strValue = " & strValue

  With Worksheets(strSheetName)
    .Label1.Caption = strValue
  End With

End Function

'*******************************************************************************
' 指定シートが存在するかチェックする
' [引数]
'   strSheetName As String：シート名
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
'*******************************************************************************
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

'*******************************************************************************
' シートを初期化する
' (Menuシート以外の全シートを削除する)
' [引数]
'   なし
' [戻り値]
'   なし
'*******************************************************************************
Public Function InitSheet()
  Dim ws As Worksheet


  With Application
    ' 警告や確認のメッセージを非表示に設定
    .DisplayAlerts = False
    ' シート名をチェックして、Main/Manualシートで無い場合、削除
    For Each ws In Worksheets
      If ((ws.Name <> MAIN_SHEET_NAME) And _
          (ws.Name <> MANUAL_SHEET_NAME) And _
          (ws.Name <> RNOTE_SHEET_NAME)) Then
        ws.Delete
      End If
    Next
    ' 設定を元に戻す
    .DisplayAlerts = True
  End With

End Function

'*******************************************************************************
' 指定シートを作成する
' (既に同名シートがある場合は、削除後に再度新規作成する)
' [引数]
'   strSheetName As String：作成するシート名
'   strAfterSheet As String：ここで指定したシート名の後ろに挿入する
' [戻り値]
'   なし
'*******************************************************************************
Public Function CreateSheet(ByVal strSheetName As String, _
                            ByVal strAfterSheet As String)
  Debug.Print "(CreateSheet):strSheetName = " & strSheetName & _
              " strAfterSheet = " & strAfterSheet

  ' 既に同名シートがある場合、削除する
  If isSheetExist(strSheetName) = True Then
    Application.DisplayAlerts = False
    Worksheets(strSheetName).Delete
    Application.DisplayAlerts = True
  End If

  Worksheets.Add After:=Worksheets(strAfterSheet)
  ActiveSheet.Name = strSheetName

  ' **** シートの書式設定 ****
  ' Font種別 = Meiryo UI
  Cells.Font.Name = "Meiryo UI"

End Function

'*******************************************************************************
' 指定ファイルの最終行数を返す
'
' [引数]
'  strFullPath As String  ：ファイル名を含むフルパス
' [戻り値]
'  Long xxx：指定ファイルの最終行数
'            ※(-1)の場合、指定ファイルが存在しない
'*******************************************************************************
Public Function GetLastLine(ByVal strFullPath As String) As Long
  Dim oFS As Object ' FileSystemObject
  Dim oTS As Object ' TextStream

  Set oFS = CreateObject("Scripting.FileSystemObject")

  ' 指定ファイルが存在しない場合は処理終了
  If (oFS.FileExists(strFullPath) = False) Then
    GetLastLine = -1
    Exit Function
  End If

  ' 追加モード(ForAppending(8))で開く
  Set oTS = oFS.OpenTextFile(strFullPath, 8)

  GetLastLine = oTS.Line - 1

End Function

'*******************************************************************************
' 指定ファイルを読込み、1ファイル1シートとして読込む
'
' [引数]
'  strFilePath As String  ：ファイルへのフルパス
'  strFileName() As String：ファイル名[配列]
' [戻り値]
'  なし
'*******************************************************************************
Public Function ReadLogFile2Sheet(ByVal strFilePath As String, _
                                  ByRef strFileName() As String)
  Dim strFullPath As String
  Dim buf As String
  Dim Temp() As String
  Dim i As Long
  Dim ReadLine As Long, LastLine As Long


  For i = 1 To UBound(strFileName)
    ' ファイルの文字コードはUTF-8として読込む
    strFullPath = strFilePath & strFileName(i)
    Debug.Print "(ReadLogFile2Sheet):strFullPath = " & strFullPath
    ' あらかじめファイルの最終行数を求めておく
    LastLine = GetLastLine(strFullPath)
    Debug.Print "(ReadLogFile2Sheet):LastLine = " & LastLine
    ' ファイル内容(全行)の格納用配列確保
    ReDim Temp(1 To LastLine)

    ' ファイル読込み
    ReadLine = 1
    With CreateObject("ADODB.Stream")
      ' Stream オブジェクトを開く
      .Open
      ' ファイルタイプ(StreamTypeEnum):adTypeText(2)
      .Type = 2
      ' 文字コード
      .Charset = "UTF-8"
      ' 改行コード(LineSeparatorsEnum):adLF(10)
      .LineSeparator = 10
      .LoadFromFile strFullPath

      Do Until .EOS
        ' 1行ずつ読込む(StreamReadEnum):adreadline(-2)
        buf = .ReadText(-2)
        ' 配列に1行づつ格納
        Temp(ReadLine) = buf
        ReadLine = ReadLine + 1
      Loop
    End With

    Debug.Print "(ReadLogFile2Sheet):ReadLine = " & ReadLine

    ' シート作成
    Call CreateSheet(Str(i), MAIN_SHEET_NAME)
    ' シートへ出力
    Worksheets(Str(i)).Range(Cells(1, 1), _
                             Cells(LastLine, 1)) = WorksheetFunction.Transpose(Temp)
  Next i

End Function

'*******************************************************************************
' 指定シート/指定行の文字列を、指定パターン(正規表現)で検索し、
' マッチした文字列を返す
'
' [引数]
'  strSheet As String    ：シート名
'  Idx As String         ：行数(対象列は1列目のみ)
'  strPattern As String  ：検索パターン(正規表現)
'  strMatches() As String：検索でマッチした文字列が格納される[配列]
' [戻り値]
'  Boolean (True) ：指定パターンにマッチした
'          (False)：指定パターンにマッチしなかった
'*******************************************************************************
Public Function RegExpSearch(ByVal strSheet As String, _
                             ByVal Idx As Long, _
                             ByVal strPattern As String, _
                             ByRef strMatches() As String) As Boolean
  Dim ws As Worksheet: Set ws = Worksheets(strSheet)
  Dim Re As Object: Set Re = CreateObject("VBScript.RegExp")
  Dim Mc As Object
  Dim i As Long
  Dim msg As String


  With Re
    .Pattern = strPattern  'パターンを指定
    .IgnoreCase = False    '大文字と小文字を区別するか(False)、しないか(True)
    .Global = True         '文字列全体を検索するか(True)、しないか(False)

    RegExpSearch = .Test(ws.Cells(Idx, 1).Value)
  End With

  If (RegExpSearch = False) Then
    Exit Function
  End If

  Set Mc = Re.Execute(ws.Cells(Idx, 1).Value)

  With Mc
    '対象文字の有無を判定
    If (.Count > 0) Then
      ReDim strMatches(.Count)

      For i = 1 To .Count
        '対象文字「有り」の場合、文字を取得
        strMatches(i) = .Item(i - 1).Value
      Next
    End If
  End With

End Function

'*******************************************************************************
' 指定文字列から、指定番目の文字列を検索して、先頭から何文字目かを返す
'
' [引数]
'  strTgt As String：検索対象文字列
'  strKey As String：検索文字列
'  pos As Long     ：求めたい検索文字列が先頭から何番目かを指定
' [戻り値]
'  Long：先頭からの位置(xx文字目)
'*******************************************************************************
Public Function InStr2(ByVal strTgt As String, _
                       ByVal strKey As String, _
                       ByVal pos As Long) As Long
  Dim i As Long
  Dim P As Long: P = 0


  For i = 1 To pos
    P = InStr(P + 1, strTgt, strKey)
  Next i

  InStr2 = P
End Function

'*******************************************************************************
' 指定文字列から特定のKeywordを検索し、結果を返す
'
' [引数]
'  Index As Long     ：格納先配列のインデックス(行数)
'  strTgt As String  ：検索対象文字列
'  strMsg() As String：検索結果の格納先配列
' [戻り値]
'  なし
'*******************************************************************************
Private Function AnalyzeMsg(ByVal index As Long, _
                            ByVal strTgt As String, _
                            ByRef strMsg() As String)
  Dim JudgeKey() As Variant
  Dim JudgePos As Long
  Dim i As Long


  JudgeKey = Array(UPPER_RQ_KEY, UPPER_RS_KEY, LOWER_RQ_KEY, LOWER_RS_KEY)

  For i = 0 To UBound(JudgeKey)
    JudgePos = InStr(strTgt, JudgeKey(i))
    ' UPPER REQUEST/UPPER RESPONSE/LOWER REQUEST/LOWER RESPONSEいずれかの場合
    If (JudgePos > 0) Then
      ' msg1に検索キー文字列を格納
      strMsg(index, eDivHead.msg1) = JudgeKey(i)
      ' msg2に検索キー以外の文字列を格納
      strMsg(index, eDivHead.msg2) = Mid(strTgt, Len(JudgeKey(i)) + 1)
      Exit Function
    End If
  Next i

  ' 上記以外の場合、msg1に検索対象文字列をそのまま格納
  strMsg(index, eDivHead.msg1) = strTgt

End Function

'*******************************************************************************
' 指定シートの列幅を調整する
'
' [引数]
'  strSheetName As String：シート名
' [戻り値]
'  なし
'*******************************************************************************
Private Function AdjustColumnWidth(ByVal strSheetName As String)
  Debug.Print "(AdjustColumnWidth):strSheetName = " & strSheetName

  With Worksheets(strSheetName)
    ' Date〜Layerまでの列幅を自動調整
    .Range(.Columns(eDivHead.Date), _
           .Columns(eDivHead.Layer)).AutoFit
    ' msg1の列幅を設定
    .Columns(eDivHead.msg1).ColumnWidth = 30
  End With

End Function

'*******************************************************************************
' 指定シートのログをログ要素に分解し、別シートに出力する
'
' [引数]
'  strSheetName As String：シート名
' [戻り値]
'  なし
'*******************************************************************************
Public Function BreakDown2LogElement(ByVal strSheetName As String)
  Dim ws As Worksheet: Set ws = Worksheets(strSheetName)
  Dim rowLast As Long
  Dim strOutSheet As String
  Dim Div() As String
  Dim strMatches() As String
  Dim strPtnDate As String: strPtnDate = "^\d{4}-\d{2}-\d{2}"
  Dim strPtnTime As String: strPtnTime = "\d{2}:\d{2}:\d{2}\.\d{3}"
  Dim strPtn1 As String: strPtn1 = "\[.*?\]"
  Dim strPtn2 As String: strPtn2 = "\[.*\].*\(.*\)\s"
  Dim strPtn3 As String: strPtn3 = "-\s\[.*"
  Dim i As Long
  Dim WriteIdx As Long: WriteIdx = 1
  Dim NgCount As Long: NgCount = 0
  Dim strFFL As String
  Dim strMsg As String


  Debug.Print "(BreakDown2LogElement):strSheetName = " & strSheetName
  rowLast = ws.Cells(Rows.Count, 1).End(xlUp).Row
  ReDim Div(1 To rowLast, eDivHead.Date To eDivHead.msg2)

  ' 1行毎に各ログ要素に分割して、配列に格納していく
  For i = 1 To rowLast
    ' 先頭のDateを検索
    If (RegExpSearch(strSheetName, i, strPtnDate, strMatches) = False) Then
      ' 無い場合は、読み飛ばす
      NgCount = NgCount + 1
      GoTo NEXT_ROW
    End If
    ' Dateを配列に保存
    Div(WriteIdx, eDivHead.Date) = strMatches(1)

    ' Timeを検索
    If (RegExpSearch(strSheetName, i, strPtnTime, strMatches) = False) Then
      ' 無い場合は、読み飛ばす
      NgCount = NgCount + 1
      GoTo NEXT_ROW
    End If
    ' Timeを配列に保存
    Div(WriteIdx, eDivHead.Time) = strMatches(1)

    ' "[]"で囲まれた文字列パターンを検索(Level/Thread1/Thread2/ThreadId/Service/Layer)
    If (RegExpSearch(strSheetName, i, strPtn1, strMatches) = False) Then
      ' 無い場合は、読み飛ばす
      NgCount = NgCount + 1
      GoTo NEXT_ROW
    End If

    ' Level/Thread1/Thread2/ThreadId/Service/Layerを配列に保存
    Div(WriteIdx, eDivHead.Level) = strMatches(1)
    Div(WriteIdx, eDivHead.Thread1) = strMatches(2)
    Div(WriteIdx, eDivHead.Thread2) = strMatches(3)
    Div(WriteIdx, eDivHead.ThreadId) = strMatches(5)
    Div(WriteIdx, eDivHead.Service) = strMatches(6)
    Div(WriteIdx, eDivHead.Layer) = strMatches(7)

    ' "[]*() "の文字列パターンを検索(FileFuncLine)
    If (RegExpSearch(strSheetName, i, strPtn2, strMatches) = False) Then
      ' 無い場合は、読み飛ばす
      NgCount = NgCount + 1
      GoTo NEXT_ROW
    End If
    ' FileFuncLineを配列に保存
    strFFL = Mid(strMatches(1), InStrRev(strMatches(1), "["))
    Div(WriteIdx, eDivHead.FileFuncLine) = strFFL

    ' "- [*"の文字列パターンを検索(msg)
    If (RegExpSearch(strSheetName, i, strPtn3, strMatches) = False) Then
      ' 無い場合は、読み飛ばす
      NgCount = NgCount + 1
      GoTo NEXT_ROW
    End If
    ' msg部分を抜き出す
    strMsg = Mid(strMatches(1), InStr2(strMatches(1), "]", 3) + 1)
    ' 先頭がスペースの場合、除去する
    If (InStr(strMsg, " ") = 1) Then
      strMsg = Mid(strMsg, 2)
    End If

    ' msg部分の分解を試み、分解できなかった場合、msg1に格納
    ' (分解できた場合、AnalyzeMsg内でmsg1, msg2に格納する)
    Call AnalyzeMsg(WriteIdx, strMsg, Div())

    ' 全ての要素が正しく抽出できていたら、
    WriteIdx = WriteIdx + 1

NEXT_ROW:
  Next i

  Debug.Print "(BreakDown2LogElement):NgCount = " & NgCount

  ' 出力用シートに配列データを出力
  strOutSheet = AS_SHEET & strSheetName
  ' シート作成
  Call CreateSheet(strOutSheet, strSheetName)
  ' シートへ出力
  With Worksheets(strOutSheet)
    .Range(.Cells(1, 1), .Cells(rowLast, eDivHead.msg2)) = Div
  End With
  ' シートの列幅を調整
  Call AdjustColumnWidth(strOutSheet)

End Function

'*******************************************************************************
' 複数出力シート(ログ要素分割済み)の内容を、1シートに統合する
'
' [引数]
'  TotalNum As Long       ：統合対象の全シート数
'  strIntegSheet As String：統合したデータ出力先のシート名
' [戻り値]
'  なし
'*******************************************************************************
Public Function IntegrateAllSheet(ByVal TotalNum As Long, _
                                  ByVal strIntegSheet As String)
  Dim ws As Worksheet, wsDist As Worksheet
  Dim i As Long
  Dim strSheetName As String
  Dim rowLast As Long, rowIntegLast As Long
  Dim rowDist As Long: rowDist = 2


  Debug.Print "(IntegrateAllSheet):TotalNum = " & TotalNum & _
              " strIntegSheet = " & strIntegSheet

  ' 作業用シート作成
  Call CreateSheet(INTEG_SHEET_NAME, MAIN_SHEET_NAME)
  Set wsDist = Worksheets(INTEG_SHEET_NAME)

  ' 作業用シートの各列に見出しをつける
  With wsDist
    .Cells(1, eDivHead.Date).Value = "Date"
    .Cells(1, eDivHead.Time).Value = "Time"
    .Cells(1, eDivHead.Level).Value = "Level"
    .Cells(1, eDivHead.Thread1).Value = "Thread1"
    .Cells(1, eDivHead.Thread2).Value = "Thread2"
    .Cells(1, eDivHead.FileFuncLine).Value = "FileFuncLine"
    .Cells(1, eDivHead.ThreadId).Value = "ThreadId"
    .Cells(1, eDivHead.Service).Value = "Service"
    .Cells(1, eDivHead.Layer).Value = "Layer"
    .Cells(1, eDivHead.msg1).Value = "msg1"
    .Cells(1, eDivHead.msg2).Value = "msg2"
    ' 全見出しを太字に設定
    .Range(.Cells(1, eDivHead.Date), _
           .Cells(1, eDivHead.msg2)).Font.Bold = True
  End With

  For i = 1 To TotalNum
    strSheetName = AS_SHEET & Str(i)
    Debug.Print "(IntegrateAllSheet):strSheetName = " & strSheetName
    Set ws = Worksheets(strSheetName)

    rowLast = ws.Cells(Rows.Count, 1).End(xlUp).Row
    Debug.Print "(IntegrateAllSheet):rowLast = " & rowLast & _
                " rowDist = " & rowDist

    ws.Range(ws.Cells(1, 1), _
             ws.Cells(rowLast, eDivHead.msg2)).Copy _
      Destination:=wsDist.Cells(rowDist, eDivHead.Date)

    ' 統合用シートのコピー先の行を更新
    rowDist = rowDist + rowLast
  Next i

  rowIntegLast = rowDist - 1
  Debug.Print "(IntegrateAllSheet):rowIntegLast = " & rowIntegLast

  ' DateをKeyにして、昇順にソート
  With wsDist
    .Range(.Cells(2, eDivHead.Date), _
           .Cells(rowIntegLast, eDivHead.msg2)).Sort _
             key1:=.Cells(2, eDivHead.Date), Order1:=xlAscending, _
             key2:=.Cells(2, eDivHead.Time), Order1:=xlAscending
    ' 見出し行で、ウィンドウ枠を固定
    .Range("A2").Select
    ActiveWindow.FreezePanes = True
  End With

  ' シートの列幅を調整
  Call AdjustColumnWidth(INTEG_SHEET_NAME)

End Function
