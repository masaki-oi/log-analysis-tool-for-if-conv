Attribute VB_Name = "TestFunc"
Option Explicit

Private Function RegSearch(ByVal strSheet As String, _
                           ByVal Idx As Long, _
                           ByVal strPattern As String, _
                           ByRef strMatches() As String) As Boolean
  Dim ws As Worksheet: Set ws = Worksheets(strSheet)
  Dim Re As Object: Set Re = CreateObject("VBScript.RegExp")
  Dim Mc As Object
  Dim i As Long
  Dim msg As String


  With Re
    .Pattern = strPattern  'パターンを指定
    .IgnoreCase = False    '大文字と小文字を区別するか(False)、しないか(True)
    .Global = True         '文字列全体を検索するか(True)、しないか(False)

    RegSearch = .Test(ws.Cells(Idx, 1).Value)
  End With

  If (RegSearch = False) Then
    Exit Function
  End If

  Set Mc = Re.Execute(ws.Cells(Idx, 1).Value)

  With Mc
    '対象文字の有無を判定
    If (.Count > 0) Then
      ReDim strMatches(.Count)

      For i = 1 To .Count
        '対象文字「有り」の場合、文字を取得
        strMatches(i) = .Item(i - 1).Value
      Next
    End If
  End With

End Function

Sub RegExpTest()
  Dim strPtn(5) As String
  Dim strMatches() As String
  Dim i As Long, j As Long
  Dim rowNum As Long: rowNum = 2

  strPtn(1) = "^\d{4}-\d{2}-\d{2}"
  strPtn(2) = "\d{2}:\d{2}:\d{2}\.\d{3}"
  strPtn(3) = "\[.*?\]"
  strPtn(4) = "\[.*\].*\(.*\)"
  strPtn(5) = "-\s\[.*"

  For i = 1 To UBound(strPtn)
    Debug.Print "(RegExpTest):strPtn(" & i & ") = " & strPtn(i)

    If (RegSearch("Test", _
                  rowNum, _
                  strPtn(i), _
                  strMatches()) = True) Then
      For j = 1 To UBound(strMatches)
        Debug.Print "(RegExpTest):strMatches(" & j & ") = " & strMatches(j)
      Next j
    Else
      Debug.Print "(RegExpTest):strPtn(" & i & ") = No Match."
    End If
  Next i

End Sub

Sub JsonParseTest()
  Dim ws As Worksheet
  Dim strJson As String
  Dim strConv As String
  Dim vConv As Variant
  Dim i As Long


  Set ws = Worksheets("Test")
  strJson = ws.Range("A3")
  Debug.Print "(JsonParseTest):strJson = " & strJson

  If (ShapingJson(strJson, strConv) = True) Then
    Debug.Print "(JsonParseTest):" & vbCrLf & strConv
  Else
    Debug.Print "(JsonParseTest): ShapingJson is failed."
  End If

  vConv = Split(strConv, vbLf)
  Debug.Print "(JsonParseTest):Split Num = " & UBound(vConv)

  For i = 0 To UBound(vConv)
    ws.Cells(5 + i, 1).Value = vConv(i)
  Next i

End Sub

Function ShapingJson(ByVal strIn As String, _
                     ByRef strOut As String) As Boolean
  Dim Parse As Object


  ' 入力文字列が""の場合、エラー終了。
  If (strIn = "") Then
    ShapingJson = False
    Debug.Print "(ShapingJson):strJson is invalid."
    Exit Function
  End If

  On Error GoTo ErrLabel
  Set Parse = JsonConverter.ParseJson(strIn)
  ' Json文字列を整形する
  strOut = JsonConverter.ConvertToJson(Parse, Whitespace:=4)

  ShapingJson = True
  Exit Function

ErrLabel:
  ' ParseJsonメソッドでエラーした場合、エラー終了。
  If (Err.Number = 10001) Then
    Debug.Print "(ShapingJson):ParseJson error.[Invalid JSON string]"
  End If

  On Error Resume Next
  Err.Clear
  ShapingJson = False

End Function

Sub VBA_JSON_TEST_ParseJson()
  Dim json As String
  Dim Parse As Object
  Dim Band_Member As Variant
  Dim Key As Variant


  'JSONのロード
  json = "{""group"":""poppin_party"",""genre"":""rock"",""member"":[""kasumi"",""tae"",""arisa"",""saaya"",""rimi""]}"
  Set Parse = JsonConverter.ParseJson(json)

  '非配列を読み出す
  Debug.Print "Group is " & Parse("group")

  '.Countを付ければ配列の要素数が出せる
  Debug.Print "member.Count = " & Parse("member").Count

  '配列を読み出す(例）
  For Each Band_Member In Parse("member")
    Debug.Print Band_Member
  Next

  '("member")(1)でkasumiがでる
  Debug.Print "1st member is " & Parse("member")(1)

  'Each Forでキーの一覧を出せる
  Debug.Print "[Json Key List]"
  For Each Key In Parse
    Debug.Print Key
  Next

End Sub

Sub VBA_JSON_TEST2_ParseJson()
  Dim json As String
  Dim Parse As Object
  Dim i As Long
  Dim Key As Variant


  'JSONのロード
  json = "{""nes_res"":{""request_id"":10,""nodes"":[{""ne_id"":""NE_1"",""error_code"":0}]}}"
  Set Parse = JsonConverter.ParseJson(json)

  '非配列を読み出す
  Debug.Print "Response = " & Parse("nes_res")("request_id")

  '.Countを付ければ配列の要素数が出せる
  Debug.Print "nodes.Count = " & Parse("nes_res")("nodes").Count

  '配列を読み出す(例）
  For i = 1 To Parse("nes_res")("nodes").Count
    Debug.Print "nes_res:nodes(" & i & ")(ne_id) = " & Parse("nes_res")("nodes")(i)("ne_id")
    Debug.Print "nes_res:nodes(" & i & ")(error_code) = " & Parse("nes_res")("nodes")(i)("error_code")
  Next

  '("member")(1)でkasumiがでる
  Debug.Print vbCrLf
  Debug.Print "1st element is " & Parse("nes_res")("nodes")(1)("ne_id")
  Debug.Print "2nd element is " & Parse("nes_res")("nodes")(1)("error_code")

  'For Eachでキーの一覧を出せる
  Debug.Print vbCrLf
  Debug.Print "[Json Key List]"
  For Each Key In Parse
    Debug.Print Key
  Next

  Debug.Print vbCrLf
  Debug.Print "[Json Key List2]"
  For Each Key In Parse("nes_res")
    Debug.Print Key
  Next

  Debug.Print vbCrLf
  Debug.Print "[Json Key List3]"
  For Each Key In Parse("nes_res")("nodes")(1)
    Debug.Print Key
  Next

  Debug.Print vbCrLf
  Debug.Print "[Formatted display Json]"
  Debug.Print JsonConverter.ConvertToJson(Parse, Whitespace:=4)

End Sub

Sub VBA_JSON_TEST3_ParseJson()
  Dim json As String
  Dim Parse As Object
  Dim i As Long
  Dim Key As Variant
  Dim strTemp As String


  'JSONのロード
  json = Worksheets("Test").Range("A3")
  Set Parse = JsonConverter.ParseJson(json)

  Debug.Print "Exists check (error_code):" & Parse("l0_pkg1_res").Exists("error_code")

  If (Parse.Count = 1) Then
    For Each Key In Parse
      strTemp = Key
      Debug.Print Key
    Next
  End If

  Debug.Print "strTemp = " & strTemp

  '非配列を読み出す
  Debug.Print "error_code = " & Parse("l0_pkg1_res")("error_code")


End Sub

Sub VBA_JSON_TEST_SendJson()
  '---------------------------------
  '　リクエストパラメタ生成
  '---------------------------------
  Dim JsonObject As Object
  Set JsonObject = New Dictionary

  JsonObject.Add "id", 1

  JsonObject.Add "name", "John Smith"

  JsonObject.Add "friend_ids", New Collection
  JsonObject("friend_ids").Add 10
  JsonObject("friend_ids").Add 20
  JsonObject("friend_ids").Add 30

  JsonObject.Add "shipTo", New Dictionary
  JsonObject("shipTo").Add "name", "Appirits Inc."
  JsonObject("shipTo").Add "address", "5F Kyocera-Harajuku Bldg. 6-27-8, Jingumae, Shibuya-ku"
  JsonObject("shipTo").Add "city", "Tokyo"
  JsonObject("shipTo").Add "state", "Japan"
  JsonObject("shipTo").Add "zip", "150-0001"

  ' イミディエイトウィンドウで確認（デバック用）
  Debug.Print JsonConverter.ConvertToJson(JsonObject, Whitespace:=2)

End Sub

Sub sample()
  ' Refer URL : https://vba-labo.rs-techdev.com/archives/1391
  Dim httpReq As New XMLHTTP60   '「Microsoft XML, v6.0」を参照設定
  Dim params As New Dictionary   '「Microsoft Scripting Runtime」を参照設定

  ' GETサンプル
  ' リクエスト情報を作成する。
  params.Add "key1", "parameterSample"
  params.Add "Key2", "日本語パラメータサンプル"
  params.Add "Key3", "% $\記号"

  With httpReq
    .Open "GET", "https://httpbin.org/get?" & encodeParams(params)
    .Send
  End With

  Do While httpReq.readyState < 4
    DoEvents
  Loop

  Debug.Print "GETレスポンス"
  Debug.Print httpReq.responseText

  'POSTサンプル
  With httpReq
    .Open "POST", "https://httpbin.org/post"
    .setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    .Send "key4=parameterSample&key5=日本語パラメータ&key6=% $\記号"
  End With

  Do While httpReq.readyState < 4
    DoEvents
  Loop

  Debug.Print "POSTレスポンス"
  Debug.Print httpReq.responseText

End Sub

' URLエンコード関数
Function encodeParams(pDic As Dictionary)
  Dim ary() As String
  ReDim ary(pDic.Count - 1) As String

  Dim i As Long
  For i = 0 To pDic.Count - 1
    ary(i) = pDic.Keys(i) & "=" & Application.WorksheetFunction.EncodeURL(pDic.Items(i))
  Next i

  encodeParams = Join(ary, "&")

End Function


