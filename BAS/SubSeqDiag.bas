Attribute VB_Name = "SubSeqDiag"
Option Explicit

'*******************************************************************************
' ログファイルが読み込まれているかチェックする
'
' [引数]
'  なし
' [戻り値]
'  Boolean：True  (ログファイルが読み込まれている)
'         ：False (ログファイルが読み込まれていない)
'*******************************************************************************
Public Function CheckLogFiles() As Boolean
  Dim ws As Worksheet


  CheckLogFiles = False

  For Each ws In Worksheets
    If (ws.Name = INTEG_SHEET_NAME) Then
      CheckLogFiles = True
      Exit Function
    End If
  Next ws

End Function

'*******************************************************************************
' シーケンス図出力用シートを作成する
'
' [引数]
'  なし
' [戻り値]
'  なし
'*******************************************************************************
Function MakeSeqSheet()
  Dim wsSeq As Worksheet


  Call CreateSheet(SEQ_SHEET_NAME, INTEG_SHEET_NAME)
  Set wsSeq = Worksheets(SEQ_SHEET_NAME)

  With wsSeq
    ' 1行目に見出しを記載
    .Cells(1, eSeqHead.Date).Value = "Date"
    .Columns(eSeqHead.Date).ColumnWidth = 15

    .Cells(1, eSeqHead.Time).Value = "Time"
    .Columns(eSeqHead.Time).ColumnWidth = 15

    .Cells(1, eSeqHead.Nexas).Value = "Nexas"
    .Columns(eSeqHead.Nexas).ColumnWidth = 45

    .Cells(1, eSeqHead.Upper).Value = "Upper"
    .Columns(eSeqHead.Upper).ColumnWidth = 7

    .Cells(1, eSeqHead.IfConv).Value = "IF-Conv"
    .Columns(eSeqHead.IfConv).ColumnWidth = 45

    .Cells(1, eSeqHead.Lower).Value = "Lower"
    .Columns(eSeqHead.Lower).ColumnWidth = 7

    .Cells(1, eSeqHead.NeOps).Value = "NE-Ops"
    .Columns(eSeqHead.NeOps).ColumnWidth = 45

    ' 全見出しを太字に設定
    .Range(.Cells(1, eSeqHead.Date), _
           .Cells(1, eSeqHead.NeOps)).Font.Bold = True
    ' 全見出しのセル色を設定
    .Range(.Cells(1, eSeqHead.Date), _
           .Cells(1, eSeqHead.NeOps)).Interior.Color = RGB(204, 255, 204)
    ' 全見出しに罫線を引く
    .Range(.Cells(1, eSeqHead.Date), _
           .Cells(1, eSeqHead.NeOps)).Borders.LineStyle = True
  End With


End Function

'*******************************************************************************
' 指定文字列(1行Json)を解析し、整形した文字列(Json)を返す
'
' [引数]
'  strIn As String ：入力文字列(1行Json)
'  strOut As String：出力文字列(整形したJson)
' [戻り値]
'  Boolean (True) ：整形出力成功
'          (False)：整形出力失敗
'*******************************************************************************
Private Function ShapingJson(ByVal strIn As String, _
                             ByRef strOut As String) As Boolean
  Dim Parse As Object


  ' 入力文字列が""の場合、エラー終了。
  If (strIn = "") Then
    ShapingJson = False
    Debug.Print "(ShapingJson):strIn is invalid."
    Exit Function
  End If

  On Error GoTo ErrLabel
  Set Parse = JsonConverter.ParseJson(strIn)
  ' Json文字列を整形する
  strOut = JsonConverter.ConvertToJson(Parse, Whitespace:=4)

  ShapingJson = True
  Exit Function

ErrLabel:
  ' ParseJsonメソッドでエラーした場合、エラー終了。
  If (Err.Number = 10001) Then
    Debug.Print "(ShapingJson):ParseJson error.[Invalid JSON string]"
  End If

  On Error Resume Next
  Err.Clear
  ShapingJson = False

End Function

'*******************************************************************************
' 指定文字列(1行Json)を解析し、error_code値を判定した結果を返す
'
' [引数]
'  strLogType As String：ログ種別
'  strJson As String ：入力文字列(1行Json)
' [戻り値]
'  Boolean (True) ：error_code正常 or error_codeが存在しない
'          (False)：error_code異常(0以外)
'*******************************************************************************
Private Function CheckJsonErrorCode(ByVal strLogType As String, _
                                    ByRef strJson As String) As Boolean
  Dim Parse As Object: Set Parse = JsonConverter.ParseJson(strJson)
  Dim ErrorCode As Long
  Dim Key As Variant
  Dim strJsonKey As String


  Select Case strLogType
  ' Upper Responseの場合
  Case UPPER_RS_KEY
    ' error_codeがJsonの最上位オブジェクトに存在する場合
    If (Parse.Exists(STR_ERROR_CODE) = True) Then
      ErrorCode = Parse(STR_ERROR_CODE)

      If (ErrorCode = 0) Then
        CheckJsonErrorCode = True
      Else
        CheckJsonErrorCode = False
      End If
    ' error_codeがJsonオブジェクトの最上位に存在しない場合
    Else
      CheckJsonErrorCode = True
    End If

  ' Lower Responseの場合
  Case LOWER_RS_KEY
    ' Jsonの最上位に1つしかオブジェクトが無い場合
    If (Parse.Count = 1) Then
      ' 最上位のオブジェクト文字列を取得
      For Each Key In Parse
        strJsonKey = Key
      Next

      ' error_codeがJsonの最上位オブジェクトの1階層目に存在する場合
      If (Parse(strJsonKey).Exists(STR_ERROR_CODE) = True) Then
        ErrorCode = Parse(strJsonKey)(STR_ERROR_CODE)

        If (ErrorCode = 0) Then
          CheckJsonErrorCode = True
        Else
          CheckJsonErrorCode = False
        End If
      ' error_codeがJsonの最上位オブジェクトの1階層目に存在しない場合
      Else
        CheckJsonErrorCode = True
      End If
    ' Jsonの最上位に1つ以上オブジェクトが有る場合
    Else
      CheckJsonErrorCode = True
    End If

  ' 上記以外の場合
  Case Else
    CheckJsonErrorCode = True
  End Select

End Function

'*******************************************************************************
' 指定ログ文字列を整形し、指定シートへ整形ログを出力する
'
' [引数]
'  strType As String     ：ログType(Upper Request/Response, Lower Request/Response)
'  strLog As String      ：整形対象ログ文字列
'  strSlogSheet As String：整形ログ出力先シート名
'  SLogIndex As Long     ：整形ログ出力先シートの出力行
' [戻り値]
'  eReSLAW - SUCCESS_NO_ERROR(0)：整形ログ出力成功(error_code正常)
'            SUCCESS_ERROR(1)   ：整形ログ出力成功(error_code異常)
'            FAIL_PARSE_ERROR(2)：整形ログ出力失敗(JsonBody部の解析失敗)
'            FAIL_NOT_JSON(3)   ：整形ログ出力失敗(JsonBody部が無い or 独自形式)
'*******************************************************************************
Private Function ShapeLogAndWrite(ByVal strType As String, _
                                  ByVal strLog As String, _
                                  ByVal strSlogSheet As String, _
                                  ByVal SLogIndex As Long, _
                                  ByRef SLogNextIndex) As eReSLAW
  Dim strShapedLog As String
  Dim vCrLfSp As Variant
  Dim i As Long, TempPos As Long
  Dim j As Long, rowEC As Long
  Dim strJson As String
  Dim strErrRange As String


  With Worksheets(strSlogSheet)
    ' 最初に整形前の1行ログを出力
    .Cells(SLogIndex, 1).Value = strLog
    i = SLogIndex + 1

    Select Case (strType)
    ' Upper Requestの場合
    Case UPPER_RQ_KEY
      ' msg2自体がJsonBody
      strJson = strLog
      ' JsonBody部を整形
      If (ShapingJson(strJson, strShapedLog) = True) Then
        vCrLfSp = Split(strShapedLog, vbCrLf)
        SLogNextIndex = i + UBound(vCrLfSp) + 1
        ' JsonBody部(整形済み)を整形ログ出力先シートへ出力
        .Range(.Cells(i, 1), _
               .Cells(SLogNextIndex, 1)) = WorksheetFunction.Transpose(vCrLfSp)
        ShapeLogAndWrite = SUCCESS_NO_ERROR
      Else
        ShapeLogAndWrite = FAIL_PARSE_ERROR
      End If

    ' Upper Responseの場合
    Case UPPER_RS_KEY
      ' msg2に",{"文字列があるか検索
      TempPos = InStr(strLog, UPPER_RS_JSON)
      If (TempPos > 0) Then
        ' JsonBody部を抜き出す
        strJson = Mid(strLog, TempPos + Len(UPPER_RS_JSON) - 1)
      Else
        ' JsonBody部が無い or 独自形式の場合、整形前の1行ログのみ出力する
        ShapeLogAndWrite = FAIL_NOT_JSON
        SLogNextIndex = i
        Exit Function
      End If
      ' JsonBody部を整形
      If (ShapingJson(strJson, strShapedLog) = True) Then
        vCrLfSp = Split(strShapedLog, vbCrLf)
        SLogNextIndex = i + UBound(vCrLfSp) + 1
        ' JsonBody部(整形済み)を整形ログ出力先シートへ出力
        .Range(.Cells(i, 1), _
               .Cells(SLogNextIndex, 1)) = WorksheetFunction.Transpose(vCrLfSp)
      Else
        ShapeLogAndWrite = FAIL_PARSE_ERROR
        Exit Function
      End If

      ' error_code解析
      If (CheckJsonErrorCode(strType, strJson) = True) Then
        ShapeLogAndWrite = SUCCESS_NO_ERROR
      Else
        ShapeLogAndWrite = SUCCESS_ERROR
      End If

    ' Lower Requestの場合
    Case LOWER_RQ_KEY
      ' msg2に"request:"文字列があるか検索
      TempPos = InStr(strLog, LOWER_RQ_JSON)
      If (TempPos > 0) Then
        ' JsonBodyを抜き出す
        strJson = Mid(strLog, TempPos + Len(LOWER_RQ_JSON))
      Else
        ShapeLogAndWrite = FAIL_NOT_JSON
        Exit Function
      End If
      ' JsonBody部を整形
      If (ShapingJson(strJson, strShapedLog) = True) Then
        vCrLfSp = Split(strShapedLog, vbCrLf)
        SLogNextIndex = i + UBound(vCrLfSp) + 1
        ' JsonBody部(整形済み)を整形ログ出力先シートへ出力
        .Range(.Cells(i, 1), _
               .Cells(SLogNextIndex, 1)) = WorksheetFunction.Transpose(vCrLfSp)
        ShapeLogAndWrite = SUCCESS_NO_ERROR
      Else
        ShapeLogAndWrite = FAIL_PARSE_ERROR
      End If

    ' Lower Responseの場合
    Case LOWER_RS_KEY
      ' msg2に",{"文字列があるか検索
      TempPos = InStr(strLog, LOWER_RS_JSON)
      If (TempPos > 0) Then
        ' JsonBody部を抜き出す
        strJson = Mid(strLog, TempPos + Len(LOWER_RS_JSON) - 1)
      Else
        ' JsonBody部が無い場合、整形前の1行ログのみ出力する
        ShapeLogAndWrite = FAIL_NOT_JSON
        SLogNextIndex = i
        Exit Function
      End If
      ' JsonBody部を整形
      If (ShapingJson(strJson, strShapedLog) = True) Then
        vCrLfSp = Split(strShapedLog, vbCrLf)
        SLogNextIndex = i + UBound(vCrLfSp) + 1
        ' JsonBody部(整形済み)を整形ログ出力先シートへ出力
        .Range(.Cells(i, 1), _
               .Cells(SLogNextIndex, 1)) = WorksheetFunction.Transpose(vCrLfSp)
      Else
        ShapeLogAndWrite = FAIL_PARSE_ERROR
        Exit Function
      End If

      ' error_code解析
      If (CheckJsonErrorCode(strType, strJson) = True) Then
        ShapeLogAndWrite = SUCCESS_NO_ERROR
      Else
        ShapeLogAndWrite = SUCCESS_ERROR
      End If

    End Select
  End With

End Function

'*******************************************************************************
' 指定ログシートを読み込み、シーケンス図＆整形ログを出力する
'
' [引数]
'  strSrcSheet As String  ：リンク元シート名
'  strSrcRange As String  ：リンク元シートRange
'  strDistSheet As String ：リンク先シート名
'  strDistRange As String ：リンク先シートRange
' [戻り値]
'  なし
'*******************************************************************************
Public Function SetLink2Sheet(ByVal strSrcSheet As String, _
                              ByVal strSrcRange As String, _
                              ByVal strDistSheet As String, _
                              ByVal strDistRange As String)
  Dim strLinkAddr As String


  strLinkAddr = strDistSheet & "!" & strDistRange

  With Worksheets(strSrcSheet)
    .Range(strSrcRange).Hyperlinks.Add Anchor:=.Range(strSrcRange), _
                                       Address:="", _
                                       SubAddress:=strLinkAddr
  End With

End Function

'*******************************************************************************
' 指定ログシートを読み込み、シーケンス図＆整形ログを出力する
'
' [引数]
'  strLogSheet As String ：ログシート名
'  strSeqSheet As String ：シーケンス図出力先シート名
'  strSlogSheet As String：整形ログ出力先シート名
' [戻り値]
'  なし
'*******************************************************************************
Public Function MakeSeqAndShapeLog(ByVal strLogSheet As String, _
                                   ByVal strSeqSheet As String, _
                                   ByVal strSlogSheet As String)
  Dim wsLog As Worksheet: Set wsLog = Worksheets(strLogSheet)
  Dim wsSeq As Worksheet: Set wsSeq = Worksheets(strSeqSheet)
  Dim wsSlog As Worksheet: Set wsSlog = Worksheets(strSlogSheet)
  Dim SeqOut() As String
  Dim strMsg1 As String, strMsg2 As String
  Dim strUpRes As String, strLoReq As String, strLoRes As String
  Dim vTmp As Variant
  Dim i As Long, rowLogMax As Long
  Dim SeqIdx As Long: SeqIdx = 1
  Dim SLogIdx As Long: SLogIdx = 1
  Dim SLogNextIdx As Long
  Dim strSrcRange As String, strDistRange As String
  Dim colSeqTgt As Long
  Dim eRtn As eReSLAW


  Debug.Print "(MakeSeqAndShapeLog):strLogSheet = " & strLogSheet & _
              " strSeqSheet = " & strSeqSheet & _
              " strSlogSheet = " & strSlogSheet

  With wsLog
    .Activate

    rowLogMax = .Cells(Rows.Count, eDivHead.Date).End(xlUp).Row
    Debug.Print "(MakeSeqAndShapeLog):rowLogMax = " & rowLogMax

    ReDim SeqOut(1 To rowLogMax, eSeqHead.Date To eSeqHead.NeOps)

    For i = 2 To rowLogMax
      ' シーケンス図出力用配列に、Date/Timeを格納
      SeqOut(SeqIdx, eSeqHead.Date) = .Cells(i, eDivHead.Date).Value
      SeqOut(SeqIdx, eSeqHead.Time) = .Cells(i, eDivHead.Time).Value

      ' msg1の文字列を解析し、場合分け処理
      strMsg1 = .Cells(i, eDivHead.msg1)
      strMsg2 = .Cells(i, eDivHead.msg2)

      ' シーケンス図出力先シートの更新
      Select Case strMsg1
      Case UPPER_RQ_KEY
        ' Upper Requestの場合、Nexas列にmsg1を格納
        SeqOut(SeqIdx, eSeqHead.Nexas) = .Cells(i, eDivHead.msg1).Value
        colSeqTgt = eSeqHead.Nexas
        ' Upper列に「'===>」を格納
        SeqOut(SeqIdx, eSeqHead.Upper) = UPPER_RQ_ARROW

        ' 整形ログ出力先シートへのリンク設定
        strSrcRange = .Cells(SeqIdx + 1, eSeqHead.Nexas).Address(False, False)
        strDistRange = .Cells(SLogIdx, 1).Address(False, False)
        Call SetLink2Sheet(strSeqSheet, strSrcRange, _
                           strSlogSheet, strDistRange)

      Case UPPER_RS_KEY
        ' Upper Responseの場合、IF-Conv列にmsg1＆
        ' msg2の2番目の空白が出現する直前までの文字列を格納
        ' 例：「<200 OK」,「<500INTERNAL_SERVER_ERROR」
        vTmp = Split(strMsg2, " ")
        strUpRes = vTmp(0) & " " & vTmp(1)
        SeqOut(SeqIdx, eSeqHead.IfConv) = strUpRes
        colSeqTgt = eSeqHead.IfConv
        ' Upper列に「'<===」を格納
        SeqOut(SeqIdx, eSeqHead.Upper) = UPPER_RS_ARROW

        ' 整形ログ出力先シートへのリンク設定
        strSrcRange = .Cells(SeqIdx + 1, eSeqHead.IfConv).Address(False, False)
        strDistRange = .Cells(SLogIdx, 1).Address(False, False)
        Call SetLink2Sheet(strSeqSheet, strSrcRange, _
                           strSlogSheet, strDistRange)

      Case LOWER_RQ_KEY
        ' Lower Requestの場合、IF-Conv列に
        ' msg2の2番目の空白が出現する直前までの文字列を格納
        ' 例：「uri:/l0_mdls_section method:POST」
        vTmp = Split(strMsg2, " ")
        strLoReq = vTmp(0) & " " & vTmp(1)
        SeqOut(SeqIdx, eSeqHead.IfConv) = strLoReq
        colSeqTgt = eSeqHead.IfConv
        ' Lower列に「'--->」を格納
        SeqOut(SeqIdx, eSeqHead.Lower) = LOWER_RQ_ARROW

        ' 整形ログ出力先シートへのリンク設定
        strSrcRange = .Cells(SeqIdx + 1, eSeqHead.IfConv).Address(False, False)
        strDistRange = .Cells(SLogIdx, 1).Address(False, False)
        Call SetLink2Sheet(strSeqSheet, strSrcRange, _
                           strSlogSheet, strDistRange)

      Case LOWER_RS_KEY
        ' Lower Responseの場合、NE-Ops列に
        ' msg2の1番目の","が出現する直前までの文字列を格納
        ' 例：「/l0_mdls_section <200」
        vTmp = Split(strMsg2, ",")
        strLoRes = vTmp(0)
        SeqOut(SeqIdx, eSeqHead.NeOps) = strLoRes
        colSeqTgt = eSeqHead.NeOps
        ' Lower列に「'--->」を格納
        SeqOut(SeqIdx, eSeqHead.Lower) = LOWER_RS_ARROW

        ' 整形ログ出力先シートへのリンク設定
        strSrcRange = .Cells(SeqIdx + 1, eSeqHead.NeOps).Address(False, False)
        strDistRange = .Cells(SLogIdx, 1).Address(False, False)
        Call SetLink2Sheet(strSeqSheet, strSrcRange, _
                           strSlogSheet, strDistRange)

      Case Else
        ' 上記以外の場合、出力しない
        GoTo NEXT_ROW
      End Select

      ' 整形ログ出力先シートの更新
      eRtn = ShapeLogAndWrite(strMsg1, strMsg2, strSlogSheet, _
                              SLogIdx, SLogNextIdx)
      Select Case (eRtn)
      Case SUCCESS_NO_ERROR
        ' NOP

      Case SUCCESS_ERROR
        ' シーケンス図出力用シートの対応するセルの背景色を「黄色」に変更
        wsSeq.Cells(SeqIdx + 1, colSeqTgt).Interior.Color = RGB(255, 255, 0)

      Case FAIL_PARSE_ERROR, FAIL_NOT_JSON
        Debug.Print "(MakeSeqAndShapeLog):eRtn = " & eRtn & _
                    " SeqIdx + 1 = " & (SeqIdx + 1) & " colSeqTgt = " & colSeqTgt
'        Debug.Print "(MakeSeqAndShapeLog):ShapeLogAndWrite Fail!" & vbCrLf & _
'                    "eRtn = " & eRtn & vbCrLf & _
'                    "strMsg1 = " & strMsg1 & vbCrLf & _
'                    "strMsg2 = " & strMsg2 & vbCrLf & _
'                    "strSlogSheet = " & strSlogSheet & vbCrLf & _
'                    "SLogIdx = " & SLogIdx
      End Select

      ' 整形ログ出力先シートの書込み行を更新
      SLogIdx = SLogNextIdx
      ' シーケンス図出力先シートの書込み行を更新
      SeqIdx = SeqIdx + 1
NEXT_ROW:
    Next i
  End With

  Debug.Print "(MakeSeqAndShapeLog):SeqIdx = " & SeqIdx & _
              " SLogIdx = " & SLogIdx

  ' ログフォーマットに準拠したログが1つも無い場合、
  ' シーケンス図出力用シートには何も出力しない
  If (SeqIdx = 1) Then
    With wsSeq
      .Activate
      ' 見出し行で、ウィンドウ枠を固定
      .Range("A2").Select
      ActiveWindow.FreezePanes = True
    End With
  Else
    ' シーケンス図出力用シートに配列内容を出力
    With Worksheets(strSeqSheet)
      .Range(.Cells(2, eSeqHead.Date), .Cells(SeqIdx, eSeqHead.NeOps)) = SeqOut

      .Activate
      ' 見出し行で、ウィンドウ枠を固定
      .Range("A2").Select
      ActiveWindow.FreezePanes = True
    End With
  End If

End Function

'*******************************************************************************
' 指定ログシートにおいて、正規表現検索でerror_codeの値検索およびHitしたセルの
' フォント色を変更する
'
' [引数]
'  strSlogSheet As String：整形ログ出力先シート名
' [戻り値]
'  なし
'*******************************************************************************
Public Function ErrorCodeCheckByRegExp(ByVal strSlogSheet As String)
  Const ECODE_OK_PTN As String = """error_code"": 0"
  Const ECODE_NG_PTN As String = """error_code"": [1-9]"
  Dim ws As Worksheet: Set ws = Worksheets(strSlogSheet)
  Dim Reg As Object: Set Reg = CreateObject("VBScript.RegExp")
  Dim i As Long
  Dim bResult As Boolean ' 検索結果
  Dim HitCnt As Long: HitCnt = 0
  Dim rowLast As Long


  Debug.Print "(ErrorCodeCheckByRegExp):strSlogSheet = " & strSlogSheet

  ' 指定された整形ログ出力先シートの最終行を求める
  rowLast = ws.Cells(Rows.Count, 1).End(xlUp).Row
  Debug.Print "(ErrorCodeCheckByRegExp):rowLast = " & rowLast

  ' 検索設定
  With Reg
    .Global = True          ' 文字列の最後まで検索（True：する、False：しない）
    .IgnoreCase = True      ' 大文字小文字の区別（True：する、False：しない）
    .Pattern = ECODE_OK_PTN ' 検索する正規表現パターン
  End With

  ' 「"error_code": 0」の文字列を検索し、フォントを青色に設定
  For i = 1 To rowLast
    ' エラーが存在するセル(#N/A)の場合、正規表現検索でエラーになるためSKIPする
    If (IsError(ws.Cells(i, 1).Value) = True) Then
      GoTo CONTINUE_OK
    End If

    bResult = Reg.Test(ws.Cells(i, 1).Value)

    ' 検索に一致しなかった場合
    If (bResult = False) Then
      GoTo CONTINUE_OK
    End If
    ' 検索に一致した場合、以下処理を行う
    'Debug.Print "(ErrorCodeCheckByRegExp):Hit address(OK) = " & _
    '            ws.Cells(i, 1).Address(False, False)
    ' Hit数をカウント
    HitCnt = HitCnt + 1
    ' Hitしたセルの文字色を青色に変更
    ws.Cells(i, 1).Font.Color = RGB(0, 0, 255)
CONTINUE_OK:
  Next i

  Debug.Print "(ErrorCodeCheckByRegExp):Hit Count(OK) = " & HitCnt
  HitCnt = 0

  ' 検索設定
  With Reg
    .Global = True          ' 文字列の最後まで検索（True：する、False：しない）
    .IgnoreCase = True      ' 大文字小文字の区別（True：する、False：しない）
    .Pattern = ECODE_NG_PTN ' 検索する正規表現パターン
  End With

  ' 「"error_code": 1-9」の文字列を検索し、フォントを赤色に設定
  For i = 1 To rowLast
    ' エラーが存在するセル(#N/A)の場合、正規表現検索でエラーになるためSKIPする
    If (IsError(ws.Cells(i, 1).Value) = True) Then
      GoTo CONTINUE_NG
    End If

    bResult = Reg.Test(ws.Cells(i, 1).Value)

    ' 検索に一致しなかった場合
    If (bResult = False) Then
      GoTo CONTINUE_NG
    End If
    ' 検索に一致した場合、以下処理を行う
    'Debug.Print "(ErrorCodeCheckByRegExp):Hit address(NG) = " & _
    '            ws.Cells(i, 1).Address(False, False)
    ' Hit数をカウント
    HitCnt = HitCnt + 1
    ' Hitしたセルの文字色を赤色に変更
    ws.Cells(i, 1).Font.Color = RGB(255, 0, 0)
CONTINUE_NG:
  Next

  Debug.Print "(ErrorCodeCheckByRegExp):Hit Count(NG) = " & HitCnt

End Function
